from django.db import models
from django.utils import timezone
from django.shortcuts import reverse

class Category(models.Model):
    title = models.CharField('Имя категории', max_length=255)
    image = models.ImageField('Картинка', blank=True, null=True, upload_to="categories/")
    slug = models.SlugField('Ссылка', unique=True)

    class Meta:
        verbose_name = "Категория"
        verbose_name_plural = "Категории"
    
    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug':self.slug})

    def __str__(self):
        return self.title

class Post(models.Model):
    title = models.CharField('Заголовок', max_length=255)
    image = models.ImageField('Картинка', blank=True, null=True, upload_to="posts/")
    slug = models.SlugField('Ссылка', unique=True)
    summary = models.TextField('Краткое описание')
    text = models.TextField('Текст')
    category = models.ForeignKey(Category, null=True, on_delete = models.CASCADE, verbose_name = "Категория")
    date = models.DateTimeField('Дата', default=timezone.now)
    views = models.IntegerField('eyes', default=0)
    
    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
    def get_absolute_url(self):
        return reverse('post_detail_url', kwargs={'slug':self.slug})

    def __str__(self):
        return self.title

class Comment(models.Model):
    author_name = models.CharField('Имя автора', max_length=255)
    text = models.TextField('Текст')
    post = models.ForeignKey(Post, null=True, on_delete = models.CASCADE, verbose_name = "")
    date = models.DateTimeField('Дата', default=timezone.now)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"
        
    def __str__(self):
        return self.author_name

class FeedBack(models.Model):
    name = models.CharField('Name Sender', max_length=255)
    phone = models.CharField('Telephone', max_length=255)
    email = models.CharField('Email', max_length=255, default = '')
    text = models.TextField('Massage')
    date = models.DateTimeField('Time', default=timezone.now)
    

    class Meta:
        verbose_name = "Massage"
        verbose_name_plural = "Massage"
    
    def __str__(self):
        return self.name